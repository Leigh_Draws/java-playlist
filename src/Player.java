import java.util.ArrayList;

public class Player {

    public static void main(String[] args) {

        Artist jackStauber = new Artist("Jack", "Stauber");
        Artist daveWol = new Artist("Dave", "Wol");
        Artist boy = new Artist("$uicide", "boy$");
        Artist geco = new Artist("J.", "Geco");
        Artist wyise = new Artist("Wyise", "Black Villen");
        Artist big = new Artist("Big", "Prodigy");

        Music breadTop = new Music("Bread", 250, jackStauber);
        Music cheese = new Music("Cheese", 322, daveWol);
        Music Lettuce = new Music("Lettuce", 358, boy);
        Music chicken = new Music("Chicken", 310, geco);
        Music mayonnaise = new Music("Mayonnaise", 124, wyise);
        Music breadBottom = new Music("Bread", 326, big);

        ArrayList<Music> playlistMusics = new ArrayList<>();
        Playlist sandwichPlaylist = new Playlist(null , playlistMusics);

        sandwichPlaylist.add(breadTop);
        sandwichPlaylist.add(cheese);
        sandwichPlaylist.add(Lettuce);
        sandwichPlaylist.add(chicken);
        sandwichPlaylist.add(mayonnaise);
        sandwichPlaylist.add(breadBottom);

        System.out.println(sandwichPlaylist.getTotalDuration());
        System.out.println(sandwichPlaylist.next());
        System.out.println(sandwichPlaylist.next());
        System.out.println(sandwichPlaylist.next());
    }
}