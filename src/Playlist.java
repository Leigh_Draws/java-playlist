import java.util.ArrayList;

public class Playlist {

    private Music currentMusic;
    private ArrayList<Music> musicList;

    // Constructeur avec currentMusic
    public Playlist(Music currentMusic, ArrayList<Music> musicList) {
        this.currentMusic = currentMusic;
        this.musicList = musicList;
    }

    public void add (Music music) {
        this.musicList.add(music);
    }

    public void remove (int position) {
        this.musicList.remove(position);
    }

    // Boucle for-each, j'avais utilisé une méthode ForEach mais je n'arrivais pas à utiliser le résultat
    public String getTotalDuration() {
        int totalDuration = 0;
        for (Music music : musicList) {
            totalDuration += music.getDuration();
        }
        int minutes = totalDuration / 60;
        int secondsLeft = totalDuration % 60;
        return "Durée totale : " + minutes + ":" + secondsLeft;
    }

    public String next() {
        if (this.currentMusic == null || this.musicList.indexOf(currentMusic) == this.musicList.size() -1 ) {
            this.currentMusic = this.musicList.getFirst();
        } else {
            int currentPosition = this.musicList.indexOf(currentMusic);
            this.currentMusic = this.musicList.get(currentPosition + 1);
        }
        return this.currentMusic.getInfo();
    }

}
