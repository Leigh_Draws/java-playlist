import java.util.ArrayList;

public class Music {

    // Attributs
    private String title;
    private int duration;
    private Artist artist;

    // Constructeur
    public Music (String title, int duration, Artist artist) {
        this.title = title;
        this.duration = duration;
        this.artist = artist;
    }

    // Méthode pour afficher les infos d'une musique. Appelle la méthode getFullName sur this.artist
    public String getInfo() {
        int minutes = this.duration / 60;
        int secondsLeft = this.duration % 60;
        return this.title + " - " + this.artist.getFullName() + ". Durée : " + minutes + " min " + secondsLeft;
    }

    // Getter
    public int getDuration() {
        return this.duration;
    }
}
