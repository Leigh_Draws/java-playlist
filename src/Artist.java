public class Artist {

    // Attributs
    private String firstName;
    private String lastName;

    // Constructeur
    public Artist(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    // Méthode
    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    // Getter / Setter
    // Getter pour accéder aux valeurs en dehors de la classe
    public String getFirstName() {
        return this.firstName;
    }

    // Setter pour modifier la valeur
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return this.lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
